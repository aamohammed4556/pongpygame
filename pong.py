import pygame, random, sys

pygame.init()
run = True
max_speed = 13


screen_x = 1600
screen_y = 800

win = pygame.display.set_mode((screen_x, screen_y))
pygame.display.set_caption('Pong')

clock = pygame.time.Clock()

ball_position_x = screen_x/2
ball_position_y = screen_y/2
ball_width = 10
ball_height = 10
ball = pygame.Rect(ball_position_x, ball_position_y, ball_width, ball_height)

player_x = screen_x - screen_x + 40
player_y = screen_y/2 - 70
player_width = 10
player_height = 140
player_score = 0
player = pygame.Rect(player_x, player_y, player_width, player_height)

player2_x = screen_x - 40
player2_y = screen_y/2 - 70
player2_width = 10
player2_height = 140
player2_score = 0
player2 = pygame.Rect(player2_x, player2_y, player2_width, player2_height)

player_vel = 9
ball_vel_x = 5
ball_vel_y = 2

font = pygame.font.SysFont('Courier', 70)

def score(player_score, player2_score, ball_position_x):
    if ball_position_x <= 10:
        player_score += 1
        
    if ball_position_x >= screen_x - ball_height:
        player2_score += 1
    
    return player_score, player2_score
    

def bounce_x(ball_vel_x):

    ball_vel_x = ball_vel_x *-1    

    if ball_vel_x > max_speed:
        ball_vel_x += -7

        return ball_vel_x

    if ball_vel_x < -max_speed:
        ball_vel_x += 7

        return ball_vel_x
        
    if ball_vel_x < 0:
        ball_vel_x += random.randint(-5, 2)

    else:
        ball_vel_x += random.randint(-2, 5)

    return ball_vel_x

def bounce_y(ball_vel_y):
    ball_vel_y = ball_vel_y *-1

    if ball_vel_y > max_speed:
        ball_vel_y += -7

    if ball_vel_y <-max_speed:
        ball_vel_y += 7

    if ball_vel_y < 0:
        ball_vel_y += random.randint(-3, 3)

    else:
        ball_vel_y += random.randint(-3, 3)

    return ball_vel_y

scoreboard = font.render('{}         {}'.format(str(player_score), str(player2_score)), True, (255, 255, 255))

########################## MAINLOOP
while run:
    pygame.time.delay(15)


    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

# all the keys
    key = pygame.key.get_pressed()

    if key[pygame.K_ESCAPE]:
        run = False
        sys.exit()

    if key[pygame.K_UP]:
        player2_y -= player_vel

    if key[pygame.K_DOWN]:
        player2_y += player_vel

    if key[pygame.K_w]:
        player_y -= player_vel
    
    if key[pygame.K_s]:
        player_y += player_vel

# Increases score and restarts game
    if ball_position_x <= 10 or ball_position_x >= screen_x - ball_height:
        player_score, player2_score = score(player_score, player2_score, ball_position_x)
        
        ball_position_x = screen_x/2
        ball_position_y = screen_y/2
        ball_vel_x = random.randint(-6, 6)
        ball_vel_y = random.randint(-6,6)  
        scoreboard = font.render('{}         {}'.format(str(player2_score), str(player_score)), True, (255, 255, 255))

        while abs(ball_vel_x) < 3 or abs(ball_vel_y) < 3:
            
            ball_vel_x = random.randint(-6, 6)
            ball_vel_y = random.randint(-6,6)

        #ball_vel_x = bounce_x(ball_vel_x)
        
    # Randomizes the y vel
        if ball_vel_y > max_speed:
            ball_vel_y += -10

        if ball_vel_y <-max_speed:
            ball_vel_y += 10

        if ball_vel_y < 0:
            ball_vel_y += random.randint(-4, 2)

        else:
            ball_vel_y += random.randint(-2, 4)       

    #bounces on y_axis
    if ball_position_y <= 20 or ball_position_y >= screen_y - ball_height - 15:
# y vel = return of bounce
        ball_vel_y = bounce_y(ball_vel_y)
# Makes a random x vel for randomness cuz why not
        if ball_vel_x > max_speed:
            ball_vel_x += -10

        if ball_vel_x < -max_speed:
            ball_vel_x += 10
            
        if ball_vel_x < 0:
            ball_vel_x += random.randint(-4, 2)

        else:
            ball_vel_x += random.randint(-2, 4)

#checks collision
    if player.colliderect(ball) or player2.colliderect(ball):
        ball_vel_x = bounce_x(ball_vel_x)

    ball_position_x += ball_vel_x
    ball_position_y += ball_vel_y

    win.fill((0, 0, 0))

    player = pygame.Rect(player_x, player_y, player_width, player_height)
    player2 = pygame.Rect(player2_x, player2_y, player2_width, player2_height)
    ball = pygame.Rect(ball_position_x, ball_position_y, ball_width, ball_height)

    pygame.draw.rect(win, (255, 255, 255), ball)

    pygame.draw.rect(win, (255, 255, 255), player)

    pygame.draw.rect(win, (255, 255, 255), player2)

    win.blit(scoreboard, ((screen_x/2) - 180, 50))

    pygame.display.update()

pygame.quit()

